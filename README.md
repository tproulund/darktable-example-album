# Example of publishing a Darktable gallery with Gitlab Pages 

This project shows how to simply add an album generated via Darktable
to the web via Gitlab Pages.  It essentially
publishing
[plain html](https://gitlab.com/pages/plain-html/tree/master).

See the result [here](https://pank.gitlab.io/darktable-example-album).
